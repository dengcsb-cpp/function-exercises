#include <iostream>
#include <string>

using namespace std;

// Declare enum. Enum is just a limited set of values.
enum HpStatus
{
	Full,
	Green,
	Yellow,
	Red,
	Dead
};

// Returns an enum which is either Full, Green, Yellow, Red, Dead
HpStatus evaluateHpStatus(int currentHp, int maxHp);

int main()
{
	// Get input for max HP
	int maxHp;
	cout << "Input max HP: ";
	cin >> maxHp;

	// Get input for current HP
	int currentHp;
	cout << "Input current HP: ";
	cin >> currentHp;

	// Check if input is valid
	if (currentHp > maxHp)
	{
		cout << "Current HP is higher than Max HP;";
		return 0;
	}

	// Get HP status based on current and max health. Store the result in an enum variable (HpStatus)
	HpStatus status = evaluateHpStatus(currentHp, maxHp);

	if (status == Full)
	{
		cout << "Full" << endl;
	}
	else if (status == Green)
	{
		cout << "Green" << endl;
	}
	else if (status == Yellow)
	{
		cout << "Yellow" << endl;
	}
	else if (status == Red)
	{
		cout << "Red" << endl;
	}
	else
	{
		cout << "Dead" << endl;
	}

	while (true)
	{
		cout << "Input 'x' to break out of loop";
		char test;
		cin >> test;

		if (test == 'x') break;
	}

	system("pause");
	return 0;
}

HpStatus evaluateHpStatus(int currentHp, int maxHp)
{
	float percentHp = (float)currentHp / maxHp;

	if (percentHp == 1.0f) return Full;
	else if (percentHp >= 0.5f) return Green;
	else if (percentHp >= 0.2f) return Yellow;
	else if (percentHp > 0) return Red;
	else return Dead;
}
