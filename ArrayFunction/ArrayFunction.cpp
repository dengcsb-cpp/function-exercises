#include <iostream>
#include <string>

using namespace std;

void printArray(string items[], int size);
bool hasItem(string items[], int size, string itemName);
int linearSearch(string items[], int size, string itemName);
int checkQuantity(string items[], int size, string itemName);
void bubbleSort(string items[], int size);

void printArray(string items[], int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << items[i] << endl;
	}
}

bool hasItem(string items[], int size, string itemName)
{
	for (int i = 0; i < size; i++)
	{
		if (items[i] == itemName)
		{
			return true;
		}
	}

	return false;
}

// Returns -1 if item does not exist
int linearSearch(string items[], int size, string itemName)
{
	for (int i = 0; i < size; i++)
	{
		if (items[i] == itemName)
		{
			return i;
		}
	}

	return -1;
}

int checkQuantity(string items[], int size, string itemName)
{
	int count = 0;
	for (int i = 0; i < size; i++)
	{
		if (items[i] == itemName)
		{
			count++;
		}
	}

	return count;
}

void bubbleSort(string items[], int size)
{
	bool swapped;
	do {
		swapped = false;
		for (int j = 0; j < size - 1; j++)
			if (items[j] > items[j + 1]) {
				string temp = items[j];
				items[j] = items[j + 1];
				items[j + 1] = temp;
				swapped = true;
			}
	} while (swapped);
}

int main()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "Red Potion", "Elixir" };
	int arraySize = 8;

	// Sort the array (bubble sort)
	bubbleSort(items, arraySize);

	// Printing an array
	cout << "Printing items in the inventory (array)..." << endl;
	printArray(items, arraySize);
	cout << endl;

	// Input item to evaluate
	cout << "Check if you have this item... ";
	string itemToSearch;
	cin >> itemToSearch;
	cout << endl;

	// Searching if an item exists in an array
	if (hasItem(items, arraySize, itemToSearch))
	{
		cout << "You have " << itemToSearch;
	}
	else
	{
		cout << "You don't have " << itemToSearch;
	}
	cout << endl;

	// Linear search. Checking if item exists and return the item OR the index of the item.
	int itemAtIndex = linearSearch(items, arraySize, itemToSearch);
	if (itemAtIndex >= 0)
	{
		cout << itemToSearch << " found at index " << itemAtIndex;
	}
	cout << endl;

	// Look for the number of instances of the item
	int itemCount = checkQuantity(items, arraySize, itemToSearch);
	if (itemCount > 0)
	{
		cout << "You have " << itemCount << " " << itemToSearch << "'s";
	}

	cout << endl << endl;
	system("pause");
	return 0;
}
