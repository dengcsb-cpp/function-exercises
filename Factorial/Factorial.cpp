#include <iostream>
#include <string>

using namespace std;

// Function prototyping - defining the function before implementing the function body. This allows you to write the function block anywhere. Without this, you always have to write the function body above the code that will use it.
int factorial(int x);

int factorial(int x)
{
	// Returning early prevents unnecessary nesting in conditional statements
	if (x < 0) return 0;

	int factorial = 1;
	while (x > 0)
	{
		factorial *= x;
		x--;
	}

	return factorial;
}
